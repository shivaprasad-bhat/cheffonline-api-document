# User Endpoints

User endpoint serves the functionalities like `Register new user`, `Login to the app`, `Get loggedin user details`, `Update User` etc.

## 1. Register API

> Method: **POST**
>
> @route: /mastercheff/api/v1.0/user

### Headers

     - None

### Request Body

-   Mandatory Fields

    -   Name as `name`
    -   Email address as `email`
    -   Password as `password`
    -   Phone Number as `contact`

-   Optional Fields

    -   Image as `photo`
    -   Admin validation (only for admin user) as `isAdmin`

### Sample Request Body

```json
{
    "name": "Test User 1",
    "email": "test1@test.com",
    "password": "123456",
    "contact": 9099090990
}
```

### Sample Output

```json
{
    "success": true,
    "data": {
        "_id": "601bb30a4a81ae059922412b",
        "name": "Test User 1",
        "email": "test2@test.com",
        "contact": 9099090990,
        "isAdmin": false,
        "photo": "user.jpg",
        "updatedAt": "2021-02-04T08:40:42.773Z",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwMWJiMzBhNGE4MWFlMDU5OTIyNDEyYiIsImlhdCI6MTYxMjQyODA0MywiZXhwIjoxNjEyNTE0NDQzfQ.eV5ogwkXEhePWWhJ_xa7ZJOEeOTXsplF_9jPjXkftzU"
    }
}
```

### Sample Error - Existing User Registration

```json
{
    "success": false,
    "message": "User already registered with this email. Please use a different email address"
}
```

### Sample Error - If any `mandatory` field is missing

```json
{
    "success": false,
    "message": "User validation failed: contact: Path `contact` is required."
}
```

---

## 2. Login API

> Method: **POST**
>
> @route: /mastercheff/api/v1.0/user/login

### Headers

    - None

### Request Body

-   Mandatory Fields

    -   Email address as `email`
    -   Password as `password`

### Sample Request Body

```json
{
    "email": "test1@test.com",
    "password": "123456"
}
```

### Sample Output

```json
{
    "success": true,
    "data": {
        "_id": "601bae4b60202e04a80292b2",
        "name": "Test User",
        "photo": "no-photo.jpg",
        "isAdmin": false,
        "updatedAt": "2021-02-04T08:20:27.384Z",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwMWJhZTRiNjAyMDJlMDRhODAyOTJiMiIsImlhdCI6MTYxMjQyOTc1OCwiZXhwIjoxNjEyNTE2MTU4fQ.nwtSY2QXo-NsgC-NwGjAIGb83lOtNe6VL8O0Tq7G7bM"
    }
}
```

### Sample Error - Try to Login with Wrong Credentials

```json
{
    "success": false,
    "message": "Invalid email / password"
}
```

## 3. Upload API

> Method: POST
> 
> @route: /mastercheff/api/v1.0/uploads/profile

### Headers
    - None

### Request Body

- Mandatory fields
    * image as `image` ( Upload a file with jpg, jpeg or png format)

### Sample output
```json
{
    "location": "/uploads/profile/image-1612588909613.jpg"
}
```

### Sample Error if no file chosen
```json
{
    "success": false,
    "message": "Cannot read property 'path' of undefined",
}
```

### Sample Error - Wrong file format chosen
```json
{
    "success": false
}
```